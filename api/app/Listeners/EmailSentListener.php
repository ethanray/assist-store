<?php

namespace App\Listeners;

use App\Traits\EmailLogger;

class EmailSentListener
{
    use EmailLogger;

    protected $type = "Sent Email";

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }
}
