<?php

namespace App\Listeners;

use App\Helpers\Wood;
use Illuminate\Database\Events\TransactionRolledBack;

class DatabaseRolledBackListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TransactionRolledBack  $event
     * @return void
     */
    public function handle(TransactionRolledBack $event)
    {
        Wood::databaseLog("============= ROLLED BACK ================" . "\n");
    }
}
