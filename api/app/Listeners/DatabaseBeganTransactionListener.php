<?php

namespace App\Listeners;

use App\Helpers\Wood;
use Illuminate\Database\Events\TransactionBeginning;

class DatabaseBeganTransactionListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TransactionBeginning  $event
     * @return void
     */
    public function handle(TransactionBeginning $event)
    {
        Wood::databaseLog("============= BEGAN TRANSACTION ================" . "\n");
    }
}
