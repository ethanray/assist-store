<?php

namespace App\Listeners;

use App\Helpers\Wood;
use Illuminate\Database\Events\TransactionCommitted;

class DatabaseCommittedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TransactionCommitted  $event
     * @return void
     */
    public function handle(TransactionCommitted $event)
    {
        Wood::databaseLog("============= COMMITTED ================" . "\n");
    }
}
