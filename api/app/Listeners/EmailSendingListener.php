<?php

namespace App\Listeners;

use App\Traits\EmailLogger;

class EmailSendingListener
{
    use EmailLogger;

    protected $type = "Sending Email";

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

}
