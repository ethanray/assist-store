<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

class Wood
{

    /**
     * Logs messages in the wood-debug.log file
     * @param mixed $message
     * @param bool $prettify
     * @return void
     */

    public static function d($message, $prettify = false)
    {
        if (env('APP_DEBUG', true)) {
            if ($prettify) {
                Log::channel('wood-debug')->debug(json_encode($message, JSON_PRETTY_PRINT) . "\n");
            } else {
                Log::channel('wood-debug')->debug($message . "\n");
            }
        }
    }

    /**
     * Logs messages in the wood-error.log file
     * @param mixed $message
     * @return void
     */
    public static function e($message)
    {
        Log::channel('wood-error')->error($message . "\n");
        Log::channel('wood-all')->error($message . "\n");
    }

    /**
     * Logs messages in the wood-info.log file
     * @param mixed $message
     * @return void
     */
    public static function i($message)
    {
        Log::channel('wood-info')->info($message . "\n");
        Log::channel('wood-all')->info($message . "\n");
    }

    /**
     * Logs messages in the wood-email.log file
     * @param mixed $message
     * @return void
     */
    public static function emailLog($message)
    {
        Log::channel('wood-email')->info($message . "\n");
        Log::channel('wood-all')->info($message . "\n");
    }

    /**
     * Logs messages in the wood-database-operation.log file
     * @param mixed $message
     * @return void
     */
    public static function databaseLog($message)
    {
        Log::channel('wood-database-operation')->info($message . "\n");
        Log::channel('wood-all')->info($message . "\n");
    }
}
