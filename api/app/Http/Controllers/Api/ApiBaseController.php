<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Traits\ResponseHandler;
use App\User;
use Illuminate\Support\Facades\Auth;

class ApiBaseController extends BaseController
{
    use ResponseHandler;

    /**
     * @param User|null $user
     * @return array
     */
    protected function _getUserWithAccessToken(User $user = null)
    {
        $user = $user == null ? Auth::user() : $user;
        return [
            'user' => $user,
            'access_token' => $user->createToken($user->username)->accessToken
        ];
    }
}
