<?php

namespace App\Http\Controllers\Api\Auth;

use App\Helpers\Security;
use App\Http\Controllers\Api\ApiBaseController;
use App\Http\Requests\RegisterRequest;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\JsonResponse;

class RegisterController extends ApiBaseController
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $request = $request->validated();
        $request['password'] = Security::hash($request['password']);
        $user = User::create($request);
        return $this->success($this->_getUserWithAccessToken($user));
    }
}
