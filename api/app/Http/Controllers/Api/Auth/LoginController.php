<?php

namespace App\Http\Controllers\Api\Auth;

use App\Helpers\Security;
use App\Http\Controllers\Api\ApiBaseController;
use App\Http\Requests\LoginRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;

class LoginController extends ApiBaseController
{

    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $username = $request->input('username');
        $user = $this->userRepository->get($username);

        if (Security::check($request->input('password'), $user->password)) {
            $response = $this->success($this->_getUserWithAccessToken($user));
        } else {
            $response = $this->error(['constants.errors.auth.invalid_credentials']);
        }

        return $response;
    }
}
