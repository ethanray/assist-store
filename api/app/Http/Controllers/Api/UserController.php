<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Security;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\GetUserInformationRequest;
use App\Repositories\UserRepository;

class UserController extends ApiBaseController
{

    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param ChangePasswordRequest $request
     * @return JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $userId = $request->input('id');
        $newPassword = $request->input('new_password');
        $user = $this->userRepository->get($userId);

        if (Security::check($request->input('current_password'), $user->password)) {
            $user->password = Security::hash($newPassword);
            if ($user->save()) {
                $response = $this->success($user);
            } else {
                $response = $this->error(['constants.errors.users.update']);
            }
        } else {
            $response = $this->error(['constants.errors.auth.invalid_credentials']);
        }

        return $response;
    }

    /**
     * @param GetUserInformationRequest $request
     * @return JsonResponse
     */
    public function getUserInformation(GetUserInformationRequest $request)
    {
        $user = $this->userRepository->getUserInformation($request->input('id'));
        return $this->success($user);
    }
}
