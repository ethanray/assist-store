<?php

namespace App\Http\Controllers\Web;

use Illuminate\Contracts\Support\Renderable;

class HomeController extends WebBaseController
{
    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('home');
    }
}
