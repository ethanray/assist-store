<?php

namespace App\Http\Middleware;

use App\Helpers\Wood;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ApiLogging
{

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = Auth::check() ? Auth::user()->getKey() : 'Not Available';

        if (env('API_LOG_REQUEST', true)) {
            $this->_logRequest($request, $userId);
        }

        $response = $next($request);

        if (env('API_LOG_RESPONSE', true)) {
            $this->_logResponse($response, $userId);
        }

        $response->headers->remove("Class-Name");

        return $response;
    }

    /**
     * Logs the request
     *
     * @param Request $request
     * @param string $userId
     */
    private function _logRequest(Request $request, $userId)
    {
        $jsonEncodeOption = env('API_LOG_REQUEST_PRETTY_PRINT', true) ? JSON_PRETTY_PRINT : 0;
        $showRequestHeader = env('SHOW_REQUEST_HEADER', true);

        $requestHeader = json_encode($request->header(), $jsonEncodeOption);
        $requestBody = json_encode($request->all(), $jsonEncodeOption);

        $dataLog = " ==============>> Request\n";
        $dataLog .= "Requested by User ID: " . $userId . "\n";
        $dataLog .= "End Point: " . $request->url() . "\n";
        $dataLog .= "Method: " . $request->route()->getActionName() . "\n";
        if ($showRequestHeader) {
            $dataLog .= "Request Header: " . $requestHeader . "\n";
        }
        $dataLog .= "Request Body: " . $requestBody . "\n\n";

        Wood::i($dataLog);
    }


    /**
     * @param JsonResponse | Response $response
     * @param string $userId
     */
    private function _logResponse($response, $userId)
    {
        $jsonEncodeOption = env('API_LOG_RESPONSE_PRETTY_PRINT', true) ? JSON_PRETTY_PRINT : 0;
        $showRequestHeader = env('SHOW_REQUEST_HEADER', true);

        $responseHeader = json_encode($response->headers->all(), $jsonEncodeOption);
        $responseBody = json_encode($response, $jsonEncodeOption);
        $processTime = floatval(microtime(true) - LARAVEL_START);

        $dataLog = " ==============>> Response\n";
        $dataLog .= "Response to User ID: " . $userId . "\n";
        $dataLog .= "Status Code: " . $response->getStatusCode() . "\n";
        $dataLog .= "Class: " . $response->headers->get("Class-Name") . "\n";
        $dataLog .= "Process Time: " . $processTime . " seconds\n";
        if ($showRequestHeader) {
            $dataLog .= "Response Header: " . $responseHeader . "\n";
        }

        if (!$response->exception) {
            $dataLog .= "Response Body: " . $responseBody . "\n\n";
        } else {
            $responseBody = json_decode($responseBody);
            $responseBody = [
                'exception' => $response->exception->errorInfo[0] ?? null,
                'message' => $response->exception->errorInfo[2] ?? null,
            ];
            $dataLog .= "Response Body: " . json_encode($responseBody, $jsonEncodeOption) . "\n\n";
        }

        Wood::i($dataLog);
    }
}
