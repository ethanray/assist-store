<?php

namespace App\Http\Requests;

use App\Helpers\Error;

class GetUserInformationRequest extends BaseFormRequest
{
    /**
     * Your own custom error messages on validation error
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => Error::bind('constants.errors.common.required'),
            'exists' => Error::bind('constants.errors.common.exists'),
        ];
    }

    /**
     * Validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:T_USER',
        ];
    }
}
