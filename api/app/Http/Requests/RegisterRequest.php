<?php

namespace App\Http\Requests;

use App\Helpers\Error;

class RegisterRequest extends BaseFormRequest
{
    /**
     * Your own custom error messages on validation error
     *
     * @return array
     */
    public function messages()
    {
        return [
            'unique' => Error::bind('constants.errors.common.unique'),
            'max' => Error::bind('constants.errors.common.max'),
            'required' => Error::bind('constants.errors.common.required'),
            'email' => Error::bind('constants.errors.common.email'),
            'confirmed' => Error::bind('constants.errors.common.confirmation'),
        ];
    }

    /**
     * Validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => "required|max:16|unique:T_USER",
            'name' => "required",
            'password' => "required|confirmed",
            'email' => "required|email|unique:T_USER"
        ];
    }
}
