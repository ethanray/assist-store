<?php

namespace App\Http\Requests;

use App\Traits\RequestValidationHandler;
use App\Traits\ResponseHandler;
use Illuminate\Foundation\Http\FormRequest;

class BaseFormRequest extends FormRequest
{
    use ResponseHandler, RequestValidationHandler;

    // replace with your own error status code
    protected $error_status_code = 422;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
