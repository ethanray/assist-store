<?php

namespace App\Http\Requests;

use App\Helpers\Error;
use App\User;

class ChangePasswordRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //Note: $user comes from the request input id
        //while $this->user() comes from the authorized user
        $user = User::find($this->input('id'));
        return $this->user()->can('changePassword', $user);
    }

    /**
     * Your own custom error messages on validation error
     *
     */
    public function messages()
    {
        return [
            'required' => Error::bind('constants.errors.common.required'),
            'exists' => Error::bind('constants.errors.common.exists'),
            'confirmed' => Error::bind('constants.errors.common.confirmation'),
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:T_USER',
            'current_password' => 'required',
            'new_password' => 'required|confirmed'
        ];
    }
}
