<?php

namespace App;

use App\Traits\ModelOperationLogger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Role extends Model
{
    use ModelOperationLogger;

    protected $table = 't_role';
    protected $primaryKey = 'id';
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * This will get all users to this role.
     *
     * @return BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(
            'App\User',
            't_user_role',
            'role_id',
            'user_id'
        );
    }

    /**
     * This will get all user_roles to this role
     *
     * @return HasMany
     */
    public function userRoles()
    {
        return $this->hasMany('App\UserRole', 'role_id', 'id');
    }
}
