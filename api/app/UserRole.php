<?php

namespace App;

use App\Traits\ModelOperationLogger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserRole extends Model
{
    use ModelOperationLogger;

    protected $table = 't_user_role';
    protected $primaryKey = 'id';
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'role_id'
    ];

    /**
     * This will return the user of user_role
     *
     * @return BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\User', 'user_id',' id');
    }

    /**
     * This will return the role of user_role
     *
     * @return BelongsTo
     */
    public function role() {
        return $this->belongsTo('App\Role', 'role_id', 'id');
    }
}
