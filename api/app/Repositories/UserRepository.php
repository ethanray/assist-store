<?php

namespace App\Repositories;

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserRepository
{
    /**
     * @param string $key
     * @return Model|Object
     */
    public function get($key)
    {
        return User::with(['roles'])->where('id', $key)
            ->orWhere('username', $key)
            ->first();
    }

    /**
     * @param string $key
     * @return Collection
     */
    public function getUserInformation($key)
    {
        return $this->get($key)
            ->only(['first_name', 'middle_name', 'last_name', 'email']);
    }
}
