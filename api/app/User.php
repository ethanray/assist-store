<?php

namespace App;

use App\Traits\ModelOperationLogger;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * @method static create(array $all)
 * @property mixed id
 */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens, ModelOperationLogger;

    protected $table = 't_user';
    protected $primaryKey = 'id';
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * This will get all the roles of the user
     *
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(
            'App\Role',
            't_user_role',
            'user_id',
            'role_id'
        );
    }
}
