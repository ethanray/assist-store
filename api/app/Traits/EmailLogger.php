<?php

namespace App\Traits;

use App\Helpers\Wood;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Mail\Events\MessageSent;

trait EmailLogger
{
    /**
     * Handle the event.
     *
     * @param MessageSending | MessageSent $event
     * @return void
     */
    public function handle($event)
    {
        if (env('MAIL_LOG', true)) {
            $dateFormat = env('MAIL_LOG_DATE_FORMAT', "F d Y (D) g:i:s a");

            $dataLog = "=============>> " . $this->type . " \n";
            $dataLog .= "Sent From: " . json_encode($event->message->getFrom()) . " \n";
            $dataLog .= "Sent to: " . json_encode($event->message->getTo()) . "\n";
            $dataLog .= "Cc: " . json_encode($event->message->getCc()) . "\n";
            $dataLog .= "Bcc: " . json_encode($event->message->getBcc()) . "\n";
            $dataLog .= "Sent on: " . $event->message->getDate()->format($dateFormat) . "\n";
            $dataLog .= "Subject: " . $event->message->getSubject() . "\n";
            Wood::emailLog($dataLog);
        }
    }
}
