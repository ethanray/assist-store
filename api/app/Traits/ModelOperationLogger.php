<?php

namespace App\Traits;

use App\Helpers\Wood;
use Illuminate\Database\Eloquent\Model;

trait ModelOperationLogger
{

    /**
     * Registers listeners to model operations 
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        if (env('DATABASE_LOG_OPERATION', true)) {
            if (env('DATABASE_LOG_RETRIEVED_OPERATION', false)) {
                static::retrieved(function ($model) {
                    ModelOperationLogger::_log("RETRIEVED", $model);
                });
            }

            if (env('DATABASE_LOG_CREATED_OPERATION', true)) {
                static::created(function ($model) {
                    ModelOperationLogger::_log("CREATED", $model);
                });
            }

            if (env('DATABASE_LOG_SAVED_OPERATION', true)) {
                static::saved(function ($model) {
                    ModelOperationLogger::_log("SAVED", $model);
                });
            }

            if (env('DATABASE_LOG_UPDATED_OPERATION', true)) {
                static::updated(function ($model) {
                    ModelOperationLogger::_logWithTable("BEFORE UPDATE", $model->getTable(), $model->getOriginal());
                    ModelOperationLogger::_logWithTable("CHANGES", $model->getTable(), $model->getChanges());
                });
            }

            if (env('DATABASE_LOG_DELETED_OPERATION', true)) {
                static::deleted(function ($model) {
                    ModelOperationLogger::_log("DELETED", $model);
                });
            }
        }
    }

    /**
     * Logs operation and model
     *
     * @param string $operation
     * @param Model $model
     * @return void
     */
    public static function _log($operation, $model)
    {
        $jsonEncodeOption = env('DATABASE_LOG_OPERATION_PRETTY_PRINT', true) ? JSON_PRETTY_PRINT : 0;

        $dataLog = " ==============>> " . $operation . "\n";
        $dataLog .= 'Table: ' . $model->getTable() . "\n";
        $dataLog .= 'Model: ' . json_encode($model, $jsonEncodeOption) . "\n";

        Wood::databaseLog($dataLog);
    }

    /**
     * Logs operation and model
     *
     * @param string $operation
     * @param string $tableName
     * @param Model $model
     * @return void
     */
    public static function _logWithTable($operation, $tableName, $model)
    {
        $jsonEncodeOption = env('DATABASE_LOG_OPERATION_PRETTY_PRINT', true) ? JSON_PRETTY_PRINT : 0;

        $dataLog = " ==============>> " . $operation . "\n";
        $dataLog .= 'Table: ' . $tableName . "\n";
        $dataLog .= 'Model: ' . json_encode($model, $jsonEncodeOption) . "\n";

        Wood::databaseLog($dataLog);
    }
}
