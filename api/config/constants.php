<?php

return [
    'security' => [
        'OPENSSL_CIPHER_NAME' => 'aes-256-cbc',
        'ENCRYPTION_KEY' => 'ajakbvakjfkadjfaqwertyuiopzxcvbn',
        'ENCRYPTION_IV' => 'V2YiUIvSR91T9DdW',
    ],
    'messages' => [
        'api' => [
            'auth' => [
                'invalid_credentials' => "Invalid Credentials"
            ],
        ],
    ],
    'errors' => [
        'key' => [
            'code_key' => 'code',
            'message_key' => 'message',
        ],
        'common' => [
            'regex' => [
                'code' => 'ECR0001',
                'message' => 'The format for :attribute is invalid',
            ],
            'max' => [
                'code' => 'ECM0002',
                'message' => '',
            ],
            'required' => [
                'code' => 'ECR0003',
                'message' => 'The :attribute field is required',
            ],
            'unique' => [
                'code' => 'ECU0004',
                'message' => 'The :attribute already exists',
            ],
            'exists' => [
                'code' => 'ECE0005',
                'message' => 'The :attribute field does not exist',
            ],
            'confirmation' => [
                'code' => 'ECC0006',
                'message' => 'Password confirmation does not match',
            ],
            'email' => [
                'code' => 'ECE0007',
                'message' => 'Email format is invalid.',
            ],
        ],
        'auth' => [
            'invalid_credentials' => [
                'code' => 'EAIC001',
                'message' => 'Invalid credentials',
            ]
        ],
        'users' => [
            'update' => [
                'code' => 'EUU0001',
                'message' => 'Failed to save.',
            ]
        ]
    ],
];
