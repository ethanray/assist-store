<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['guest'])->group(function () {
    Route::get('login', 'Web\Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Web\Auth\LoginController@login');

    Route::get('password/reset', 'Web\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Web\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Web\Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Web\Auth\ResetPasswordController@reset')->name('password.update');

    Route::get('register', 'Web\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Web\Auth\RegisterController@register');
});

Route::middleware(['auth'])->group(function () {
    Route::post('logout', 'Web\Auth\LoginController@logout')->name('logout');
    Route::get('/home', 'Web\HomeController@index')->name('home');
});

