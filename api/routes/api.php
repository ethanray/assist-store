<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*ACCOUNT*/
Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'Api\Auth\LoginController@login')->name('auth.login');
    Route::post('/register', 'Api\Auth\RegisterController@register')->name('auth.register')->middleware('transaction');
});

Route::group(['prefix' => 'user'], function () {
    Route::patch('/change-password', 'Api\UserController@changePassword')->middleware('auth:api', 'transaction');
    Route::get('/info', 'Api\UserController@getUserInformation')->middleware('auth:api');
});

