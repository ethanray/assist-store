import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Image } from "react-native";

import CashierRegisterScreen from "../screens/CashierRegisterScreen";
import HomeScreen from "../screens/HomeScreen";
import InventoryScreen from "../screens/InventoryScreen";
import ChecklistScreen from "../screens/ChecklistScreen";
import ReportsScreen from "../screens/ReportsScreen";

const Drawer = createDrawerNavigator();

const AppNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: "Home",
          drawerIcon: (config) => (
            <Image source={require("../../assets/assist_store.png")} />
          ),
        }}
      />
      <Drawer.Screen
        name="CashRegister"
        component={CashierRegisterScreen}
        options={{
          title: "Cash Register",
          drawerIcon: (config) => (
            <Image source={require("../../assets/cashier.png")} />
          ),
        }}
      />
      <Drawer.Screen
        name="Inventory"
        component={InventoryScreen}
        options={{
          drawerIcon: (config) => (
            <Image source={require("../../assets/inventory.png")} />
          ),
        }}
      />
      <Drawer.Screen
        name="CheckList"
        component={ChecklistScreen}
        options={{
          drawerIcon: (config) => (
            <Image source={require("../../assets/checklist.png")} />
          ),
        }}
      />
      <Drawer.Screen
        name="Reports"
        component={ReportsScreen}
        options={{
          drawerIcon: (config) => (
            <Image source={require("../../assets/progress_report.png")} />
          ),
        }}
      />
    </Drawer.Navigator>
  );
};

export default AppNavigator;
