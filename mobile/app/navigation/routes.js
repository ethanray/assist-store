export default Object.freeze({
  LOGIN: "Login",
  REGISTER: "Register",
  HOME: "Home",
  CASHIER_REGISTER: "CashierRegister",
  INVENTORY: "Inventory",
  CHECKLIST: "Checklist",
  REPORTS: "Reports",
});
