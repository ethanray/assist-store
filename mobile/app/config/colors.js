export default {
  primary: "#A458C5",
  secondary: "#7E61C9",
  black: "#000",
  white: "#fff",
  medium: "#6e6969",
  light: "#f8f4f4",
  dark: "#0c0c0c",
  danger: "#ff5252",
};
