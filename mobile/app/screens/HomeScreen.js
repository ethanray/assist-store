import React from "react";
import { View, StyleSheet, Text } from "react-native";

function HomeScreen(props) {
  return (
    <View style={styles.container}>
      <Text>Home</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
});

export default HomeScreen;
