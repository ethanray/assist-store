import React, { useState } from "react";
import * as Yup from "yup";
import { StatusBar } from "expo-status-bar";
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  Text,
} from "react-native";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import Screen from "../components/Screen";
import routes from "../navigation/routes";

const validationSchema = Yup.object().shape({
  username: Yup.string().required().label("Username"),
  password: Yup.string().required().label("Password"),
});

function LoginScreen({ navigation }) {
  const [loginFailed, setLoginFailed] = useState(false);

  const handleLogin = () => alert("Submit Login and Navigate to Main Menu");
  const handleRegister = () => navigation.navigate(routes.REGISTER);

  const handleSubmit = async ({ email, password }) => {
    const result = await authApi.login(email, password);
    if (!result.ok) return setLoginFailed(true);
    setLoginFailed(false);
    auth.logIn(result.data);
  };

  return (
    <Screen style={styles.container}>
      <StatusBar style="auto" />
      <TouchableWithoutFeedback onLongPress={handleRegister}>
        <Image source={require("../../assets/icon.png")} style={styles.logo} />
      </TouchableWithoutFeedback>
      <Text style={styles.title}>Assist Store</Text>
      <View style={styles.formContainer}>
        <Form
          initialValues={{ username: "", password: "" }}
          onSubmit={handleLogin}
          validationSchema={validationSchema}
        >
          <ErrorMessage
            error="Invalid username and/or password."
            visible={loginFailed}
          />
          <FormField
            autoCapitalize="none"
            autoCorrect={false}
            icon="account"
            name="username"
            placeholder="Username"
          />
          <FormField
            autoCapitalize="none"
            autoCorrect={false}
            icon="lock"
            name="password"
            placeholder="Password"
            secureTextEntry
            textContentType="password"
          />
          <SubmitButton title="Login" />
        </Form>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
  formContainer: {
    paddingHorizontal: 15,
    width: "100%",
  },
  logo: {
    alignSelf: "center",
    height: 200,
    width: 200,
  },
  title: {
    alignSelf: "center",
    fontWeight: "bold",
    fontSize: 30,
    marginBottom: 30,
  },
});

export default LoginScreen;
